package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class IndexTest {
    private WebDriver driver;

    String file ="src/test/resources/login_data.csv";


    @BeforeEach
    public void setUp() {


       //    WebDriver driver9 = new ChromeDriver();
        this.driver =new ChromeDriver();
           this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(6));
           this.driver.get("https://www.zverimexaja.cz/");
    }
    @Test
    public void successLogin(){

        driver.navigate().to("https://www.zverimexaja.cz/login/");
        WebElement clickCookie = driver.findElement(By.className("siteCookies__button"));
        clickCookie.click();

        WebElement textInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(2) > div > input"));
        textInput.sendKeys("notino1@post.cz");

        WebElement passwordInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(3) > div > input.form-control"));
        passwordInput.sendKeys("poiu");

        WebElement submit= driver.findElement(By.cssSelector("#formLogin > div:nth-child(4) > div > button"));
        submit.click();

        String expectedMessage = "Klientské centrum\n" +
                "Ještě jste žádnou objednávku nevytvořil....";
        String actualMessage = driver.findElement(By.className("content-inner")).getText();
        Assertions.assertEquals(expectedMessage, actualMessage);




    }

    @Test
    public void blankPass(){
        driver.navigate().to("https://www.zverimexaja.cz/login/");
        WebElement clickCookie = driver.findElement(By.className("siteCookies__button"));
        clickCookie.click();

        WebElement textInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(2) > div > input"));
        textInput.sendKeys("notino1@post.cz");

        WebElement passwordInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(3) > div > input.form-control"));
        passwordInput.sendKeys("poiu");

        WebElement submit= driver.findElement(By.cssSelector("#formLogin > div:nth-child(4) > div > button"));
        submit.click();
        driver.navigate().to("https://www.zverimexaja.cz/nastaveni/");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(6));
//        WebElement textInput= driver.findElement(By.cssSelector("#billFullName"));
//        textInput.sendKeys(" ");



        WebElement submit1= driver.findElement(By.cssSelector("#register-form > div.form-group.submit-wrapper > p > input"));
        submit1.click();

        String expectedMessage = "Povinné pole";
        String actualMessage = driver.findElement(By.cssSelector("#register-form > div:nth-child(1) > fieldset > div:nth-child(5) > div")).getText();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void diffPass(){
        driver.navigate().to("https://www.zverimexaja.cz/login/");
        WebElement clickCookie = driver.findElement(By.className("siteCookies__button"));
        clickCookie.click();

        WebElement textInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(2) > div > input"));
        textInput.sendKeys("notino1@post.cz");

        WebElement passwordInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(3) > div > input.form-control"));
        passwordInput.sendKeys("poiu");

        WebElement submit= driver.findElement(By.cssSelector("#formLogin > div:nth-child(4) > div > button"));
        submit.click();

        driver.navigate().to("https://www.zverimexaja.cz/klient/nastaveni");
        WebElement textInput5= driver.findElement(By.cssSelector("#currentPassword"));
        textInput5.sendKeys("poiu");
        WebElement passInput= driver.findElement(By.cssSelector("#password"));
        passInput.sendKeys("poiuii");
        WebElement pass2Input= driver.findElement(By.cssSelector("#passwordAgain"));
        pass2Input.sendKeys("poiuoo");



        WebElement submit6= driver.findElement(By.cssSelector("body"));
        submit6.click();

        String expectedMessage = "Hesla se neshodují";
        String actualMessage = driver.findElement(By.className("validator-msg")).getText();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void shopFirma(){

        driver.navigate().to("https://www.zverimexaja.cz/login/");
        WebElement clickCookie = driver.findElement(By.className("siteCookies__button"));
        clickCookie.click();

        WebElement textInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(2) > div > input"));
        textInput.sendKeys("notino1@post.cz");

        WebElement passwordInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(3) > div > input.form-control"));
        passwordInput.sendKeys("poiu");

        WebElement submit= driver.findElement(By.cssSelector("#formLogin > div:nth-child(4) > div > button"));
        submit.click();
        driver.navigate().to("https://www.zverimexaja.cz/klient/nastaveni");


        WebElement submit3= driver.findElement(By.cssSelector("#additionalInformation > fieldset > div.form-group.company-shopping.unveil-wrapper > label"));
        submit3.click();

        String expectedMessage = "IČ";
        String actualMessage = driver.findElement(By.cssSelector("#company-info > div:nth-child(1) > label")).getText();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }
    @Test
    public void addToCart(){
        driver.navigate().to("https://www.zverimexaja.cz/login/");

        WebElement clickCookie = driver.findElement(By.className("siteCookies__button"));
        clickCookie.click();

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(6));

        WebElement textInput1= driver.findElement(By.cssSelector("#formLogin > div:nth-child(2) > div > input"));
        textInput1.sendKeys("notino1@post.cz");

        WebElement passwordInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(3) > div > input.form-control"));
        passwordInput.sendKeys("poiu");

        WebElement submit1= driver.findElement(By.cssSelector("#formLogin > div:nth-child(4) > div > button"));
        submit1.click();


        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(6));

        WebElement textInput3= driver.findElement(By.cssSelector("#formSearchForm > fieldset > input.query-input.form-control.search-input.js-search-input"));
        textInput3.sendKeys("potkan");


        WebElement submit= driver.findElement(By.cssSelector("#formSearchForm > fieldset > button"));
        submit.click();

        String expectedMessage = "Kategorie\n" +
                "Hlodavci a králíci";
        String actualMessage = driver.findElement(By.cssSelector("#search-group-categories")).getText();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/login_data.csv", numLinesToSkip = 1)
    public void testIncorrectLogin(String email, String password) {

        driver.navigate().to("https://www.zverimexaja.cz/login/");
        WebElement textInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(2) > div > input"));
        textInput.sendKeys(email);

        WebElement passwordInput= driver.findElement(By.cssSelector("#formLogin > div:nth-child(3) > div > input.form-control"));
        passwordInput.sendKeys(password);

        WebElement submit6= driver.findElement(By.xpath("//*[@id=\"formLogin\"]/div[3]/div/button"));
        submit6.click();

        String expectedMessage = "Přihlášení zákazníka";
        String actualMessage = driver.findElement(By.xpath("//*[@id=\"content\"]/div/h1")).getText();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/search_data.csv", numLinesToSkip = 1)
    public void testSearch(String product) {


        WebElement textInput= driver.findElement(By.xpath("//*[@id=\"formSearchForm\"]/fieldset/input[2]"));
        textInput.sendKeys(product);


        WebElement submit6= driver.findElement(By.xpath("//*[@id=\"formSearchForm\"]/fieldset/button"));
        submit6.click();

        String expectedMessage = product;
        String actualMessage = driver.findElement(By.xpath("//*[@id=\"products-found\"]/div/div[1]/div/div/div[1]/a/span")).getText();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/url_data.csv", numLinesToSkip = 1)
    public void testURL(String message, String message2) {




        WebElement submit6= driver.findElement(By.xpath("//*[text()='"+message+"']"));
        submit6.click();



        Assertions.assertEquals(driver.getCurrentUrl(), message2);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

}
